const express = require("express");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/ping", (req, res) => {
  res.send({ message: "pong", code: 200 });
});

app.get("/", (req, res) => {
  res.send({ message: "working perfect", code: 200 });
});

let port = 9001;
app.listen(port, (_) => console.log(`server listening on port ${port}`));
